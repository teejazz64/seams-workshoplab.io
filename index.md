---
layout: default
---
{::options parse_block_html="true" /}
<div class="home">{% include applications.md %}
Welcome to the ***Software Engineering for Applied Mathematical Sciences*** (***SEAMS*** for short) site!  We are now preparing for our next [*SEAMS Workshop*](workshop) on **20-31 January 2020** in partnership with [SACEMA](http://www.sacema.org/).

At SEAMS, we prepare scientists for the challenges of computationally-oriented research program. We have two offerings: the *Principles* course covering our five topic focuses and the *Workshop* which incorporates a hack-a-thon **on your project** in addition to the course material.

The most recent *Workshop* was held from 26 November-7 December, 2018 in Muizenberg, Cape Town, South Africa, with [AIMS South Africa](https://www.aims.ac.za/).  We will be offering the *Workshop* again 20-31 January 2020 in Stellenbosch, South Africa, with SACEMA.  We hope to offer the *Principles* course some time in June or July of 2020.

For information on early iterations of the workshop, please see the {% include oldlink.md tx='old website' %}.  [See our most recent participants](participants/2018/) and [their feedback on the program](past).

***SEAMS*** has been made possible by contributions from {% include lo.md tx='AIMS' l='https://www.nexteinstein.org/' %} {% include lo.md tx='South Africa' l='https://aims.ac.za/' %} and {% include lo.md tx='Ghana' l='https://aims.edu.gh/' %}, {% include lo.md tx='ONR' l='https://www.onr.navy.mil/' %} and {% include lo.md tx='USAFRICOM' l='https://www.africom.mil' %}, {% include lo.md tx='University of Florida' l='https://biology.ufl.edu/' %}, {% include lo.md tx='SACEMA' l='http://www.sacema.org' %}, and other partners.
</div>

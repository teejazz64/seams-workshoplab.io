---
slug: io
title: Input & Output (IO)
---

# Code vs Input

* Does your project have "code" currently that might alternatively be thought of as input?  E.g., parameter or configuration values that are included directly in your scripts, instead of in input files.
* For cases where you do, do you find yourself regularly changing those values, e.g. to run a new scenario?  If other people were to use your code, might they want to change those values?

# Kinds of Input & Output

* List your inputs and outputs, and label those data with their best fit as tabular, relational, hierarchical, key-value, or human-oriented (e.g., a plot or sound file).
* For your inputs and outputs, which are general purpose formats (e.g., csv, json, sqlite, protobuf) with libraries for many languages, specialized (e.g., rds, docx) with application-specific access, or custom (e.g., human readable, but with specialized parsing rules)?  What are your reasons for using general vs specialized vs custom formats?
* For your inputs and outputs, which are human-readable (plaintext) vs machine-only readable (binary)?  What are your reasons for using plaintext vs binary formats for different parts of your project?

# Intermediate Results

* Compared to your project overall outputs, are there intermediate files that are only used internally to your project?  Where do they fall relative to the overall process steps you outlined on Monday?
* Does your project have a "resume" capability?  If so, how do you implement it?  If not, how might it benefit from one?
* Do you cache any inputs?  If so, why?

# Testing & Validation

* Which of your project requirements specifically concern an input or output file feature?  E.g., must read / write particular format, size, ...?  Or more generally the creation or consumption of specific data in specific files?
* Does your project include in testing of inputs or outputs?  E.g., validation of inputs, checking for missing data, confirmation of output consistency with some independent standard, ...?

# Amounts & Locations

* How much input does your project rely on?  Estimate in kilobytes, Mbs, Gbs, (Tbs?) as appropriate.
* ...how much output does it create?
* Is the input used or output created by your project all local?  Or does some travel over a network?
* How much time does your project spend loading data--e.g., reading in a csv vs how much time the program spends analyzing the information in the file?
* How much time does your project spend writing data--e.g., generating and saving a plot vs generating the time series being plotted.

# Sharing

* Are your raw inputs shared?  Cleaned up inputs?  Why or why not?
* How do your collaborators manage data?  Which services or processes did your group consider before settling on the current one?
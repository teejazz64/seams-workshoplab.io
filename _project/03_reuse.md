---
slug: reuse
title: Reuse & Reusability
---

# Re-use activities to prepare for work on your project 

## Parts of your project that may be reusable

On day 1 (project design) we talked about modularity, and you made a process map for your project.  Think about that as you begin these exercises.  
 
* Prepare to become a package author.  
   * find out how and where packages are distributed in your preferred language, e.g., CRAN for R.  
   * create an account
* Imagine carving off part of your code as a re-usable package. 
   * before thinking about other stakeholders, think about your future self.  How could some parts of your code be used by yourself in other projects?  
   * who are your target users or stakeholders, beyond yourself?  What about your immediate co-workers?   What about other people in the same field? In other fields?  What is going to make your module attractive to them?  
   * Write a few sentence about what your package does.  
   * search the web for similar packages in the same or other languages.  What did you learn? 
*  Research the mechanics of package generation in your preferred language
   * identify a source of guidelines or best practices and bookmark that
   * install any tools that you need to generate a package 
*  Recall your choices yesterday for workspace organization  
   * now that you know more about package generation for your preferred language, are they any particulars about filesystem arrangement?  How do they work with (or against) choices you made yesterday?
   * In addition to directory structure, are there are system configuration elements (e.g., environment variables, program aliases) that are required for packaging?
*  Do a quick cost-benefit analysis.   You have imagined a package and explored the tools and best practices for generating and distributing packages.  
   * what extra work is required for you to make a package? 
   * what are the costs and benefits? 

## Parts of your project that might be replaced with existing modules

This assumes that you know how to find packages for your preferred language and platform.  

*  Identify replaceable components in your project.  
   * list 3 functionalities of your code that someone has probably implemented before.  Maybe there is a better version out there that would improve your project.    
   * poke around on the web.  Try to find a module that does what you want. 
   * what extra functionality would you get from importing this module (e.g., handling errors or exceptions)?  Is it going to make your life easier?  how do you decide whether importing this is worth the effort?   
* Map out the steps to replace part of your code with an imported module
   * does this add further dependencies?  is that a problem?  How do you decide? 
   * Will you wrap imported functions (methods) to match yours, or rewrite your code to call imported functions directly? 
   * how will you declare this dependency to potential users?
 
## Ways to extend your project with existing modules

This assumes that you know how to find packages for your preferred language and platform.  

*  Write down 3 ways that you might want to extend your project with new functionality.  Explore ways to implement that functionality using existing modules.  For instance
   * IO.  Maybe you want to store outputs in a database instead of ad hoc files.  Maybe you you want to input JSON instead of ad hoc files.  Are there modules for that?  
   * estimation methods.  Are there some other ways to do parameter estimation, perhaps more powerful or flexible than what you are doing?  
   * graphical user interfaces.  Would your code benefit from a web interface?  What's the easiest way to do that?
* Thinking about other packages your hypothetical package might be used with - what are their interfaces?  Is your interface written with the same approach?  What new way of thinking would potential users have to learn to combine your library with others?  Are there ways to make your interface more similar to that of other libraries a scientist would commonly use alongside your work?

## Reuse Setting

* Where do you imagine your code being reused?  As a command line tool, as a package within some larger program, ...?  What are the implications of those settings?

## Testing

* How testing is relevant to reuse?  If someone wants to extend your work (or you want to extend other work), how can you make sure you don't break that code?
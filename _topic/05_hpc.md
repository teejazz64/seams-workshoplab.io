---
slug: hpc
title: High Performance Computing
---

A working understanding of the physical parts of a computer is important for making decisions about how to make code faster.
- [Here's a basic overview of the parts of a modern desktop computer](http://sites.jmu.edu/103oconnor-16/introduction-to-basic-computer-components-and-functions/)  If you've never built a computer, this might be helpful.
- [How are supercomputers different?](https://en.wikipedia.org/wiki/Supercomputer_architecture)
- [What is cache, and why does it speed things up?](https://softwareengineering.stackexchange.com/questions/234253/why-is-cpu-cache-memory-so-fast)
- [How big should a page file/swap partition be?](https://www.howtogeek.com/196238/how-big-should-your-page-file-or-swap-partition-be/)

Why is my code slow?
- [Profilers can help you identify code bottlenecks](https://stackify.com/what-is-code-profiling/)
- [One developer's story of how he used unit testing concepts to benchmark his code](https://bruun.co/2012/02/07/easy-cpp-benchmarking)
- [Big-O notation and algorithmic complexity--aka, maybe your algorithm is the problem](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Algorithmic%20Complexity/complexity.html)
- [A proof of Amdahl's Law, which predicts how much a program can be sped up when parallelized](https://www.geeksforgeeks.org/computer-organization-amdahls-law-and-its-proof/)

So you want to parallelize . . .
- [A phenomenal introduction to parallel computing](https://computing.llnl.gov/tutorials/parallel_comp/)
- [An intuitive explanation of race conditions: how many people does it take to turn off a light?](https://searchstorage.techtarget.com/definition/race-condition)
- [Multithreading is an approach to parallelization, but doesn't always result in parallelization](https://stackoverflow.com/questions/806499/threading-vs-parallelism-how-do-they-differ)
- [An overview of cloud computing (not a how-to)](https://www.dialogic.com/~/media/products/docs/whitepapers/12023-cloud-computing-wp.pdf)
- [MPI: tutorials for how to parallelize in hardcore mode, generally for compiled languages](http://mpitutorial.com/tutorials/)
- Map-Reduce, Spread-Gather - some sort of link to embarassingly parallel problem framings
- [Difference between CPU and GPU computing](https://medium.com/altumea/gpu-vs-cpu-computing-what-to-choose-a9788a2370c4)
- [GPU computing, it's strengths and limitations](http://lorenabarba.com/gpuatbu/Program_files/Cruz_gpuComputing09.pdf)

<!--Some reference reading:

 - [An Introduction to Mathematical Modeling](http://www.maths.bris.ac.uk/~madjl/course_text.pdf)
 - [Agent Based Models](http://www.palgrave-journals.com/jos/journal/v4/n3/full/jos20103a.html)
 - [Compartmental Models and Mathematical Epidemiology](http://www.springer.com/cda/content/document/cda_downloaddocument/9783540789109-c1.pdf?SGWID=0-0-45-532715-p173817706)
 - Other modeling techniques, software, code packages:
    * [pycx](http://pycx.sourceforge.net/) ([about pycx](http://www.casmodeling.com/content/1/1/2))
-->